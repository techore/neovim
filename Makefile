## Makefile downloads from git repository then unarchives file. Use
## createdeb.sh to build debian package.

srcurl=https://github.com/neovim/neovim/releases/download/v0.9.2/nvim-linux64.tar.gz
srcout=$(shell basename ${srcurl})
srcdir=linux64

all:
	test -f ${srcout} || wget ${srcurl}
	tar xvf ${srcout}
	mv nvim-linux64 ${srcdir}

clean:
	rm -fr ${srcout} ${srcdir}
